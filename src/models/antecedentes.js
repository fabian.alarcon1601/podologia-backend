import {Sequelize} from 'sequelize';
import {sequelize} from '../database/database';
import FichaClinica from './ficha_clinica'

const Antecedentes = sequelize.define('antecedentes',{
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement:true
    },
    ingiere_alcohol:{
        type: Sequelize.STRING,
        allowNull: true
    },
    fuma:{
        type: Sequelize.STRING,
        allowNull: true
    },
    medicamentos:{
        type: Sequelize.STRING,
        allowNull: true
    },
    alergias:{
        type: Sequelize.STRING,
        allowNull: true
    },
    enfermedades_cronicas:{
        type: Sequelize.STRING,
        allowNull: true
    },
    traumatismo:{
        type: Sequelize.STRING,
        allowNull: true
    },
    operaciones:{
        type: Sequelize.STRING,
        allowNull: true
    },
    ficha_clinica_id:{
        type: Sequelize.INTEGER,
        references: {
            model: FichaClinica,
            key: ' id'
        },
    }
},{
    timestamps: false,
    tableName: 'antecedentes'
});

export default Antecedentes;