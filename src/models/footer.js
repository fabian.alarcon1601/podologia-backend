import { Sequelize } from 'sequelize';
import { sequelize } from '../database/database';


const Footer = sequelize.define('footer', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    instagram: {
        type: Sequelize.STRING,
        allowNull: false
    },
    facebook: {
        type: Sequelize.STRING,
        allowNull: false
    },
    whatsapp: {
        type: Sequelize.STRING,
        allowNull: false
    },
    numero_telefono: {
        type: Sequelize.STRING,
        allowNull: false
    },
    numero_celular: {
        type: Sequelize.STRING,
        allowNull: false
    }
},{
    timestamps: false,
    tableName: 'footer'
});

export default Footer;