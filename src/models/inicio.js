import { Sequelize } from 'sequelize';
import { sequelize } from '../database/database';


const Inicio = sequelize.define('inicio', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    mision: {
        type: Sequelize.STRING,
        allowNull: false
    },
    vision: {
        type: Sequelize.STRING,
        allowNull: false
    },
    valorBasica: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    valorAvanzada: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    valorClinica: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    curacion: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    reconstruccion: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    titulo: {
        type: Sequelize.STRING,
        allowNull: false
    },
    subTitulo: {
        type: Sequelize.STRING,
        allowNull: false
    }
},{
    timestamps: false,
    tableName: 'inicio'
});

export default Inicio;