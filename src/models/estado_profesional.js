import {Sequelize} from 'sequelize';
import {sequelize} from '../database/database';
import Profesional from './profesional';

const Estado_profesional = sequelize.define('Estado_profesional',{
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement:true
    },
    nombre:{
        type: Sequelize.STRING,
        allowNull: false
    }
},{
    timestamps: false,
    tableName: 'estado_profesional'
});

Estado_profesional.hasMany(Profesional, {
    foreignKey: {
        name: 'estado_profesional_id',
    },
    sourceKey: 'id'
});
Profesional.belongsTo(Estado_profesional, {
    foreignKey: {
        name: 'estado_profesional_id',
    },
    sourceKey: 'id'
});

export default Estado_profesional;