import {Sequelize} from 'sequelize';
import {sequelize} from '../database/database';
import FichaClinica from '../models/ficha_clinica';

const Diagnostico = sequelize.define('diagnostico',{
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement:true
    },
    motivo_consulta:{
        type: Sequelize.STRING,
        allowNull: false
    },
    diagnostico:{
        type: Sequelize.STRING,
        allowNull: false
    },
    fecha:{
        type: Sequelize.DATEONLY,
        allowNull: false
    },
    ficha_clinica_id:{
        type: Sequelize.INTEGER,
        references: {
            model: FichaClinica,
            key: ' id'
        },
    }
},{
    timestamps: false,
    tableName: 'diagnostico'
});

export default Diagnostico;