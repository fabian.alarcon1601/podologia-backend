import {Sequelize} from 'sequelize';
import {sequelize} from '../database/database';
import EstadoReserva from './estado_reserva';
import Profesional from './profesional';
import Paciente from './paciente';

const Reserva = sequelize.define('reserva',{
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement:true
    },
    fecha:{
        type: Sequelize.DATEONLY,
        allowNull: false
    },
    hora:{
        type: Sequelize.STRING,
        allowNull: false
    },
    pofesional_id:{
        type: Sequelize.INTEGER,
        references: {
            model: Profesional,
            key: ' id'
        },
    },
    paciente_id: {
        type: Sequelize.INTEGER,
        references: {
            model: Paciente,
            key: ' id'
        },
    },
    estado_reserva_id: {
        type: Sequelize.INTEGER,
        references: {
            model: EstadoReserva,
            key: ' id'
        },
    }
},{
    timestamps: false,
    tableName: 'reserva'
});

//relaciones 


EstadoReserva.hasMany(Reserva, {
    foreignKey: {
        name: 'estado_reserva_id',
    },
    sourceKey: 'id'
});
Reserva.belongsTo(EstadoReserva, {
    foreignKey: {
        name: 'estado_reserva_id',
    },
    sourceKey: 'id'
});

Paciente.hasMany(Reserva, {
    foreignKey: {
        name: 'paciente_id',
    },
    sourceKey: 'id'
});
Reserva.belongsTo(Paciente, {
    foreignKey: {
        name: 'paciente_id',
    },
    sourceKey: 'id'
});

export default Reserva;