import {Sequelize} from 'sequelize';
import {sequelize} from '../database/database';
import FichaClinica from './ficha_clinica';

const Historial = sequelize.define('historial_sesiones',{
    id : {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    fecha: {
        type: Sequelize.DATEONLY,
        allowNull: false
    },
    asistencia: {
        type : Sequelize.BOOLEAN,
        allowNull: false
    },
    ficha_clinica_id:{
        type: Sequelize.INTEGER,
        references: {
            model: FichaClinica,
            key: ' id'
        },
    }
},{
    timestamps: false,
    tableName: 'historial_sesiones'
});

export default Historial;