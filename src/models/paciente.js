import { Sequelize } from 'sequelize';
import { sequelize } from '../database/database';
import Comuna from './comuna'
import FichaClinica from './ficha_clinica';

const Paciente = sequelize.define('paciente', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nombres: {
        type: Sequelize.STRING,
        allowNull: false
    },
    apellido_paterno: {
        type: Sequelize.STRING,
        allowNull: false
    },
    apellido_materno: {
        type: Sequelize.STRING,
        allowNull: false
    },
    correo: {
        type: Sequelize.STRING,
        allowNull: false
    },
    telefono: {
        type: Sequelize.STRING,
        allowNull: false
    },
    rut: {
        type: Sequelize.STRING,
        allowNull: false
    },
    n_emergencia: {
        type: Sequelize.STRING,
        allowNull: true
    },
    comuna_id:{
        type: Sequelize.INTEGER,
        references: {
            model: Comuna,
            key: ' id'
        },
    }

}, {
    timestamps: false,
    tableName: 'paciente'
});

Paciente.hasOne(FichaClinica, {
    foreignKey: {
        name: 'paciente_id'
    },
    sourceKey: 'id'
})

FichaClinica.belongsTo(Paciente, {
    foreignKey: {
        name: 'paciente_id'
    },
    sourceKey: 'id'
});

Comuna.hasMany(Paciente, {
    foreignKey: {
        name: 'comuna_id'
    },
    sourceKey: 'id'
})

Paciente.belongsTo(Comuna, {
    foreignKey: {
        name: 'comuna_id'
    },
    sourceKey: 'id'
});


export default Paciente;