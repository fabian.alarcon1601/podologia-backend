import {Sequelize} from 'sequelize';
import {sequelize} from '../database/database';
import Profesional from '../models/profesional';

const Usuario = sequelize.define('usuario',{
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement:true
    },
    correo: {
        
        type: Sequelize.STRING,
        allowNull:false,
        validate:{
            notNull:{
                msg: 'Ingresa un correo'
            },
            isEmail: {
                msg: 'Ingresa un correo valido'}
        }
    },
    pass: {
        type: Sequelize.STRING,
        allowNull:false
    },
    pass_recovery: {
        type: Sequelize.STRING,
        allowNull:true
    },
    token: {
        type: Sequelize.STRING,
        allowNull:true
    },
    nombres: {
        type: Sequelize.STRING,
        allowNull:false
    },
    apellido_paterno: {
        type: Sequelize.STRING,
        allowNull:false
    },
    apellido_materno: {
        type: Sequelize.STRING,
        allowNull:false
    },
    rut: {
        type: Sequelize.STRING,
        allowNull:false
    },
    telefono: {
        type: Sequelize.STRING,
        allowNull:false
    },
    rol_id: {
        type: Sequelize.INTEGER,
    }
},{
    timestamps: false,
    tableName: 'usuario'
});



//Definición relación profesional
Usuario.hasOne(Profesional, {
    foreignKey: {
        name: 'usuario_id',
    },
    sourceKey: 'id'
});
Profesional.belongsTo(Usuario, {
    foreignKey: {
        name: 'usuario_id',
    },
    sourceKey: 'id'
});

export default Usuario;