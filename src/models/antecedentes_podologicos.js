import {Sequelize} from 'sequelize';
import {sequelize} from '../database/database';
import FichaClinica from '../models/ficha_clinica'

const Antecedentes_podo = sequelize.define('antecedentes_podologicos',{
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement:true
    },
    problemas_ortopedicos:{
        type: Sequelize.STRING,
        allowNull: false
    },
    n_calzado:{
        type: Sequelize.INTEGER,
        allowNull: false
    },
    amputaciones:{
        type: Sequelize.STRING,
        allowNull: false
    },
    ulceraciones:{
        type: Sequelize.STRING,
        allowNull: false
    },
    sensibilidad:{
        type: Sequelize.STRING,
        allowNull: false
    },
    heridas:{
        type: Sequelize.STRING,
        allowNull: false
    },
    pulso_pedio:{
        type: Sequelize.STRING,
        allowNull: false
    },
    ficha_clinica_id:{
        type: Sequelize.INTEGER,
        references: {
            model: FichaClinica,
            key: ' id'
        },
    }
},{
    timestamps: false,
    tableName: 'antecedentes_podologicos'
});

export default Antecedentes_podo;