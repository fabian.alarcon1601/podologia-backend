import {Sequelize} from 'sequelize';
import {sequelize} from '../database/database';
import Reserva from './reserva';

const EstadoReserva = sequelize.define('estado_reserva',{
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement:true
    },
    estado:{
        type: Sequelize.STRING,
        allowNull: false
    }
},{
    timestamps: false,
    tableName: 'estado_reserva'
});



export default EstadoReserva;