import { Sequelize } from 'sequelize';
import { sequelize } from '../database/database';
import Paciente from "./paciente"

const Comuna = sequelize.define('comuna', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nombre: {
        type: Sequelize.STRING,
        allowNull: false
    }
},{
    timestamps: false,
    tableName: 'comuna'
});




export default Comuna;