import express, {json, urlencoded} from 'express';
import morgan from 'morgan';

import usuarioRoutes from '../src/routes/usuario';
import pacienteRoutes from '../src/routes/paciente';
import profesionalRoutes from '../src/routes/profesional';
import estadoProfesionalRoutes from '../src/routes/estado_profesional';
import rolRoutes from '../src/routes/rol';
import fichas_clinicasRoutes from '../src/routes/ficha_clinica';
import historial_sesionesRoutes from '../src/routes/historial_sesiones';
import especialidadRoutes from '../src/routes/especialidad';
import estado_reservaRoutes from '../src/routes/estado_reserva';
import horarioRoutes from '../src/routes/horario';
import reservaRoutes from '../src/routes/reserva';
import estado_horarioRoutes from '../src/routes/estado_horario';
import comunaRoutes from '../src/routes/comuna';
import antecedentesRoutes from '../src/routes/antecedentes';
import antecedentes_podologicosRoutes from '../src/routes/antecedentes_podologicos';
import diagnosticoRoutes from '../src/routes/diagnostico';
import inicioRoutes from '../src/routes/inicio';
import footerRoutes from '../src/routes/footer';

//importing routes

//initialization
const app  = express();

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

//middlewares
app.use(morgan('dev'));
app.use(urlencoded({
    extended: true
}));
app.use(json());

//routes 
app.use('/api/usuario', usuarioRoutes);
app.use('/api/paciente', pacienteRoutes);
app.use('/api/profesional', profesionalRoutes);
app.use('/api/estado', estadoProfesionalRoutes);
app.use('/api/rol', rolRoutes);
app.use('/api/fichas',fichas_clinicasRoutes);
app.use('/api/historial',historial_sesionesRoutes);
app.use('/api/especialidad', especialidadRoutes);
app.use('/api/estadoReserva', estado_reservaRoutes);
app.use('/api/horario', horarioRoutes);
app.use('/api/reserva', reservaRoutes);
app.use('/api/estado/horario',estado_horarioRoutes);
app.use('/api/estado/reserva', estado_reservaRoutes);
app.use('/api/antecedentes', antecedentesRoutes);
app.use('/api/antecedentes/podologicos', antecedentes_podologicosRoutes);
app.use('/api/diagnostico', diagnosticoRoutes);
app.use('/api/inicio', inicioRoutes);
app.use('/api/footer', footerRoutes);
app.use('/api/comuna', comunaRoutes);

export default app;