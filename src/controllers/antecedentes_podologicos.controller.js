import Antecedentes_podo from "../models/antecedentes_podologicos";
import FichaClinica from "../models/ficha_clinica";

export async function listarAntecedentesPod(req, res) {
    let AntecedentePod = await Antecedentes_podo.findAll({
        include:[{
            model: FichaClinica,
            attributes: ['id']
        }],
        attributes: ['id', 'n_calzado', 'problemas_ortopedicos', 'amputaciones', 'ulceraciones', 'sensibilidad', 'pulso_pedio', 'heridas'],
        order: [
            ['id', 'DESC']
        ]
    });
    res.json({
        code: 200,
        message: 'Listados Con Exito',
        Antecedentes_podo: AntecedentePod
    });
}


export async function crearAntecedentesPod(req, res) {
    const {
        n_calzado,
        problemas_ortopedicos,
        amputaciones,
        ulceraciones,
        sensibilidad,
        pulso_pedio,
        heridas,
        ficha_clinica_id
    } = req.body;
    try {
        let nuevoAntecedentePod = await Antecedentes_podo.create({
            n_calzado,
            problemas_ortopedicos,
            amputaciones,
            ulceraciones,
            sensibilidad,
            pulso_pedio,
            heridas,
            ficha_clinica_id
        })
        if (nuevoAntecedentePod) {
            res.json({
                code: 200,
                message: 'Antecedente Podológico Creadao Con Exito',
                data: nuevoAntecedentePod
            });
        }
    } catch (e) {
        res.json({
            code: 400,
            message: 'Error al crear el antecedente podológico',
            error: e.errors
        });
    }
}

export async function editarAntecedentesPod(req, res) {
    const {
        n_calzado,
        problemas_ortopedicos,
        amputaciones,
        ulceraciones,
        sensibilidad,
        pulso_pedio,
        heridas
    } = req.body;
    if (n_calzado && problemas_ortopedicos && amputaciones && ulceraciones && sensibilidad && pulso_pedio && heridas) {
        try {
            let nuevoAntecedentePod = await Antecedentes_podo.findOne({
                where: {
                    id: id
                },
                attributes: ['id', 'n_calzado', 'problemas_ortopedicos', 'amputaciones', 'ulceraciones', 'sensibilidad', 'pulso_pedio', 'heridas']
            });
            if (nuevoAntecedentePod) {
                nuevoAntecedentePod.update({
                    n_calzado,
                    problemas_ortopedicos,
                    amputaciones,
                    ulceraciones,
                    sensibilidad,
                    pulso_pedio,
                    heridas
                });
                res.json({
                    code: 200,
                    message: 'Se ha editado correctamente',
                    data: nuevoAntecedentePod
                });
            } else {
                res.json({
                    code: 400,
                    message: 'No se encontro el antecedente podologico',
                });
            }
        } catch (e) {
            console.log(e.errors);
            res.json({
                code: 401,
                message: 'ERROR',
            });
        }
    } else {
        res.json({
            code: 203,
            message: 'INGRESE DATOS PARA EDITAR'
        });
    }
}