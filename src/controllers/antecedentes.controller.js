import Antecedentes from "../models/antecedentes";
import FichaClinica from "../models/ficha_clinica";

export async function listarAntecedentes(req, res) {
    let antecedentes = await Antecedentes.findAll({
        include:[{
            model: FichaClinica,
            attributes: ['id']
        }],
        attributes: ['id', 'ingiere_alcohol', 'operaciones', 'fuma', 'medicamentos', 'alergias', 'enfermedades_cronicas', 'traumatismo'],
        order: [
            ['id', 'DESC']
        ]
    });
    res.json({
        code: 200,
        message: 'Listados Con Exito',
        Antecedentes: antecedentes
    });
}


export async function crearAntecedentes(req, res) {
    const {
        ingiere_alcohol,
        operaciones,
        fuma,
        medicamentos,
        alergias,
        enfermedades_cronicas,
        traumatismo,
        ficha_clinica_id
    } = req.body;
    try {
        let nuevaAntecedente = await Antecedentes.create({
            ingiere_alcohol,
            operaciones,
            fuma,
            medicamentos,
            alergias,
            enfermedades_cronicas,
            traumatismo,
            ficha_clinica_id
        })
        if (nuevaAntecedente) {
            res.json({
                code: 200,
                message: 'Antecedentes Creados Con Exito',
                data: nuevaAntecedente
            });
        }
    } catch (e) {
        res.json({
            code: 400,
            message: 'Error al crear los antecedentes',
            error: e.errors
        });
    }
}

export async function editarAntecedentes(req, res) {
    const {
        ingiere_alcohol,
        operaciones,
        fuma,
        medicamentos,
        alergias,
        enfermedades_cronicas,
        traumatismo
    } = req.body;
    if (ingiere_alcohol && operaciones && fuma && medicamentos && alergias && enfermedades_cronicas && traumatismo && ficha_clinica_id) {
        try {
            let nuevoAntecedente = await Antecedentes.findOne({
                where: {
                    id: id
                },
                include: [{
                    model: FichaClinica,
                    attributes: ['id']
                }],
                attributes: ['id', 'ingiere_alcohol', 'operaciones', 'fuma', 'medicamentos', 'alergias', 'enfermedades_cronicas', 'traumatismo']
            });
            if (nuevoAntecedente) {
                nuevoAntecedentePod.update({
                    ingiere_alcohol,
                    operaciones,
                    fuma,
                    medicamentos,
                    alergias,
                    enfermedades_cronicas,
                    traumatismo
                });
                res.json({
                    code: 200,
                    message: 'Se ha editado correctamente',
                    data: nuevoAntecedente
                });
            } else {
                res.json({
                    code: 400,
                    message: 'No se encontro el antecedente ',
                });
            }
        } catch (e) {
            console.log(e.errors);
            res.json({
                code: 401,
                message: 'ERROR',
            });
        }
    } else {
        res.json({
            code: 203,
            message: 'INGRESE DATOS PARA EDITAR'
        });
    }
}