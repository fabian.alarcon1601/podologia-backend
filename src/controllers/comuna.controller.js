import comuna from "../models/comuna";

export async function listarComuna(req, res){
    let Comuna= await comuna.findAll({
        attributes:['id','nombre'],
        order:[['id','DESC']]
    });
    res.json({
        code:200,
        message: 'Listados Con Exito',
        Comuna : Comuna
    });
}


export async function crearComuna(req, res){
    const {nombre, comuna_id} = req.body;
    try{
        let nuevaComuna = await comuna.create({
            nombre,
            comuna_id
        })
        if(nuevaComuna){
            res.json({
                code:200,
                message: 'Comuna Creada Con Exito',
                data : nuevaComuna
            });
        }
    }catch(e){
        res.json({
            code:400,
            message: 'Error al crear la comuna',
            error: e.errors
        });
    }
}