// para cargar el backend del horario lo que hay que hacer es recorrer
// por la cantidad de dias que se mande desde el body, primer loop, for dias
// luego hay que recorrer por la cantidad de horas en un intervalo estatico de una hora
// parametros a pedir
// los meses que va a cargar o es 1 o es muchos capturarlos en numeros, donde 1 indica enero, 1-7 indicando de enero a julio
// el intevalo de dias es decir 1 lunes y 1-5 de lunes a viernes
// el intervalo de horas donde los valores sean el inicio 9:00 y el final 15:00 en intervalos de 1 hora.
// for meses
// for dias
// for horas
//create horario

import Horario from "../models/horario";
import Profesional from "../models/profesional";
import EstadoHorario from "../models/estado_horario";



export async function cargarHorario(req, res) {
    const {
        mesInicio,
        mesFinal,
        diaInicio,
        diaFinal,
        horaInicio,
        horaFinal,
        profesional_id
    } = req.body;
    console.log(mesInicio, mesFinal, diaInicio, diaFinal, horaInicio, horaFinal, profesional_id);
    let estado_horario_id = '1';

    try {
        if (mesInicio != undefined && mesFinal != undefined && diaInicio != undefined && diaFinal != undefined && horaInicio != undefined && horaFinal != undefined) {
                console.log("Primer if");
                console.log("Segundo if");
                let anio = new Date().getFullYear();
                let mes = 0;
                let diasMes = 1;

                let diasSemana = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
                let resta = horaFinal - horaInicio;
                
                //Se recorre desde el mes de inicio hasta el mes final aumentando mes a mes

                for (mes = mesInicio; mes <= mesFinal; mes++) {
                    console.log("1 for ");
                    //obtiene la cantidad de dias del mes ingresado por parametro
                    diasMes = new Date(anio, mes, 0).getDate();
                    //Recorre los dias del mes 
                    for (let dia = 1; dia <= diasMes ; dia++) {
                        console.log("2 for ");
                        //Recorre los dias seleccionados 
                        for (let i = diaInicio; i <= diaFinal; i++) {
                            console.log("Este es i: "+ i );
                            console.log("3 for ");
                            let indice = new Date(anio, mes - 1, dia).getDay();
                            console.log(diasSemana[indice]);
                            console.log(diasSemana[i]);
                            if (diasSemana[indice] === diasSemana[i]) {
                                console.log("Tercer if");
                                //Ingreso de horas
                                for (let k = 0; k < resta; k++) {
                                    console.log("4 for ");
                                    let fecha = '';
                                    let mes_asignar = '';
                                    let dia_asignar = '';
                                    if (mes < 10) {
                                        console.log("Cuarto If mes");
                                        mes_asignar = '0' + mes;
                                    } else {
                                        console.log("5 If mes");
                                        mes_asignar = mes;
                                    }
                                    if (dia < 10) {
                                        console.log("6 If mes");
                                        dia_asignar = '0' + dia;
                                    } else {
                                        console.log("7 If mes");
                                        dia_asignar = dia;
                                    }

                                    fecha = anio + '-' + mes_asignar + '-' + dia;
                                    let hora_asignar = (parseInt(horaInicio) + k);
                                    let hora_incrementar = hora_asignar;
                                    if (hora_asignar <= 9) {
                                        console.log("8 If mes");
                                        hora_incrementar = '0' + hora_asignar;
                                    }
                                    let hora = hora_incrementar + ':00';
                                    await Horario.create({
                                        hora,
                                        fecha,
                                        profesional_id,
                                        estado_horario_id
                                    });
                                }
                            }
                        }
                    }
                
            }
            res.json({
                code: 200,
                message: 'Horario cargado corectamente',
            });


        }
        res.json({
            code: 400,
            message: 'Faltan datos por ingresar',
        });
        

    } catch (e) {
        console.log(e);
        res.json({
            code: 400,
            message: 'Error al crear el horario',
            error: e.errors
        });
    }
}

export async function buscarDisponibilidad(req, res) {

    const {
        fecha,
        profesional_id
    } = req.body;
    console.log(JSON.stringify(req.body));
    try {
        let horario = await Horario.findAll({
            where: {
                fecha,
                profesional_id,
                estado_horario_id:1
            },
            attributes: ['id', 'fecha', 'hora'],
            include: [{
                model: EstadoHorario,
                attributes: ['nombre']
            }],
        });
        if (horario) {
            res.json({
                code: 200,
                message: 'El horario ha sido encontrado con éxito',
                data: horario
            });
        } else {
            res.json({
                code: 400,
                message: 'El horario no existe'
            });
        }
    } catch (e) {
        console.log(e);
        res.json({
            code: 401,
            message: 'ERROR'
        });
    }
}