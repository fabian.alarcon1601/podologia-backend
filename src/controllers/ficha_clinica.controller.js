import FichaClinica from "../models/ficha_clinica";
import Paciente from "../models/paciente";
import Comuna from "../models/comuna";
import Antecedentes from "../models/antecedentes";
import Antecedentes_podo from "../models/antecedentes_podologicos";
import HistorialSesiones from "../models/historial_sesiones";
import Diagnosticos from "../models/diagnostico";

export async function crearFicha(req, res) {
    const {
        paciente_id
    } = req.body;

    let dia = new Date().getDate();
    let mes = new Date().getMonth() + 1;
    let anio = new Date().getFullYear();
    let fecha_ingreso = '';
    if (mes < 10) {
        mes = '0' + mes;
    }
    if (dia < 10) {
        dia = '0' + dia;
    }
    fecha_ingreso = anio + '-' + mes + '-' + dia;

    try {
        let nuevaFichaClinica = await FichaClinica.create({
            fecha_ingreso,
            paciente_id
        })
        if (nuevaFichaClinica) {
            res.json({
                code: 200,
                message: 'Ficha Clinica creada correctamente',
                data: nuevaFichaClinica
            });
        }

    } catch (e) {
        res.json({
            code: 400,
            message: 'Error al crear ficha clinica',
            error: e.errors
        });
    }
}

export async function listarFichas(req, res) {
    try {
        let fichas = await FichaClinica.findAll({
            include: [{
                model: Paciente,
                attributes: ['nombres', 'apellido_paterno', 'apellido_materno', 'correo', 'rut', 'telefono']
            }],
            attributes: ['id', 'fecha_ingreso'],
            order: [
                ['id', 'DESC']
            ]
        });
        res.json({
            code: 200,
            message: 'Fichas clinicas listadas correctamente',
            FichasClinicas: fichas
        });
    } catch (e) {
        res.json({
            code: 400,
            message: 'Error al listar las fichas clinicas',
            error: e
        });
    }
}

export async function buscarFichas(req, res) {
    const {
        id
    } = req.body;
    try {
        let fichaClinica = await FichaClinica.findOne({
            where: {
                id: id
            },
            include: [{
                model: Paciente,
                attributes: ['nombres', 'apellido_paterno', 'apellido_materno', 'correo', 'rut', 'telefono']
            }],
            attributes: ['id', 'fecha_ingreso']
        });
        if (fichaClinica) {
            res.json({
                code: 200,
                message: 'La ficha ha sido encontrada con exito',
                data: fichaClinica
            });
        } else {
            res.json({
                code: 400,
                message: 'La ficha clinica no existe'
            });
        }
    } catch (e) {
        res.json({
            code: 401,
            message: 'ERROR'
        });
    }
}

export async function buscarFichaPorPaciente(req, res) {
    const {
        id
    } = req.body;
    try {
        let fichaClinica = await FichaClinica.findOne({
            where: {
                paciente_id: id
            },
            include: [{
                model: Paciente,
                attributes: ['nombres', 'apellido_paterno', 'apellido_materno', 'correo', 'rut', 'telefono','n_emergencia'],
                include:[{
                    model: Comuna,
                    attributes: ['nombre']
                }]
            }],
            attributes: ['id', 'fecha_ingreso'],
            
        });
          
        if (fichaClinica) {
            //Realizar consulta en los modelos de antecedentes y antecedentespod con la ficha_clinica_id = id de ficha clinica
            let antecedentes = await Antecedentes.findOne({
                where: {
                    ficha_clinica_id:fichaClinica.id 
                }, 
                attributes: ['id', 'ingiere_alcohol', 'operaciones', 'fuma', 'medicamentos', 'alergias', 'enfermedades_cronicas', 'traumatismo']
            });
            
            let antecedentes_podologicos= await Antecedentes_podo.findOne({
                where: {
                    ficha_clinica_id:fichaClinica.id 
                }, 
                attributes: ['id', 'n_calzado', 'problemas_ortopedicos', 'amputaciones', 'ulceraciones', 'sensibilidad', 'pulso_pedio', 'heridas']
            });

            let historialSesiones= await HistorialSesiones.findAll({
                where: {
                    ficha_clinica_id:fichaClinica.id 
                }, 
                attributes: ['id', 'fecha', 'asistencia']
            });

            let diagnostico= await Diagnosticos.findAll({
                where: {
                    ficha_clinica_id:fichaClinica.id 
                }, 
                attributes: ['id','fecha','diagnostico','motivo_consulta']
            });

            
                res.json({
                    code: 200,
                    message: 'La ficha ha sido encontrada con exito',
                    data: fichaClinica,
                    antecedentes:antecedentes,
                    antecedentespodologicos:antecedentes_podologicos,
                    historialSesiones,
                    diagnostico
                });
                        
            
        } else {
            res.json({
                code: 400,
                message: 'La ficha clinica no existe'
            });
        }
    } catch (e) {
        console.log("aqui el mensaje de sqlmesage -----------------------------------------------------"+ e);
        res.json({
            code: 401,
            message: e.parent.sqlMessage
        });
    }
}



export async function editarFicha(req, res) {
    const {
        id,
        fecha_ingreso
    } = req.body;
    if (fecha_ingreso) {
        try {
            let fichaClinica = await FichaClinica.findOne({
                where: {
                    id: id
                },
                attributes: ['id', 'fecha_ingreso']
            });
            if (fichaClinica) {
                fichaClinica.update({
                    fecha_ingreso
                });
                res.json({
                    code: 200,
                    message: 'Se ha editado correctamente',
                    data: fichaClinica
                });
            } else {
                res.json({
                    code: 400,
                    message: 'No se encontro la ficha clinica',
                });
            }
        } catch (e) {
            console.log(e.errors);
            res.json({
                code: 401,
                message: 'ERROR',
            });
        }
    } else {
        res.json({
            code: 203,
            message: 'INGRESE DATOS PARA EDITAR'
        });
    }
}