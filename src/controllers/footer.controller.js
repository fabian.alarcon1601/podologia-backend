import Footer from "../models/footer";

export async function listarFooter(req, res) {
    let footer = await Footer.findOne({
        attributes: ['id', 'instagram', 'facebook', 'whatsapp', 'numero_telefono', 'numero_celular'],
        order: [
            ['id', 'DESC']
        ]
    });
    res.json({
        code: 200,
        message: 'Listados Con Exito',
        Footer: footer
    });
}

export async function crearFooter(req, res) {
    const {
        instagram,
        facebook,
        whatsapp,
        numero_telefono,
        numero_celular
    } = req.body;
    try {
        let nuevoFooter = await Footer.create({
            instagram,
            facebook,
            whatsapp,
            numero_telefono,
            numero_celular
        })
        if (nuevoFooter) {
            res.json({
                code: 200,
                message: 'Footer Creado Con Exito',
                Footer: nuevoFooter
            });
        }
    } catch (e) {
        res.json({
            code: 400,
            message: 'Error al crear el footer',
            error: e.errors
        });
    }
}

export async function editarFooter(req, res) {
    const {
        id,
        instagram,
        facebook,
        whatsapp,
        numero_telefono,
        numero_celular
    } = req.body;
    if (instagram && facebook && whatsapp && numero_telefono && numero_celular ) {
        try {
            let footer = await Footer.findOne({
                where: {
                    id: id
                },
                attributes: ['id', 'instagram', 'facebook', 'whatsapp', 'numero_telefono', 'numero_celular' ]
            });
            if (footer) {
                footer.update({
                    instagram,
                    facebook,
                    whatsapp,
                    numero_telefono,
                    numero_celular,
                });
                res.json({
                    code: 200,
                    message: 'Se ha editado el footer con exito',
                    data: footer
                });
            } else {
                res.json({
                    code: 400,
                    message: 'No existe footer'
                });
            }
        } catch (e) {
            res.json({
                code: 401,
                message: 'ERROR'
            });
        }
    }
}