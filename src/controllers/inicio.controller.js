import Inicio from "../models/inicio";

export async function listarInicio(req, res) {
    let inicio = await Inicio.findOne({
        attributes: ['id', 'mision', 'vision', 'valorBasica', 'valorAvanzada', 'valorClinica', 'curacion', 'reconstruccion','titulo', 'subTitulo'],
        order: [
            ['id', 'DESC']
        ]
    });
    res.json({
        code: 200,
        message: 'Listados Con Exito',
        Inicio: inicio
    });
}

export async function crearInicio(req, res) {
    const {
        mision,
        vision,
        valorBasica,
        valorAvanzada,
        valorClinica,
        curacion,
        reconstruccion,
        titulo,
        subTitulo
    } = req.body;
    try {
        let nuevoInicio = await Inicio.create({
            mision,
            vision,
            valorBasica,
            valorAvanzada,
            valorClinica,
            curacion,
            reconstruccion,
            titulo,
            subTitulo
        })
        if (nuevoInicio) {
            res.json({
                code: 200,
                message: 'Inicio Creado Con Exito',
                Inicio: nuevoInicio
            });
        }
    } catch (e) {
        res.json({
            code: 400,
            message: 'Error al crear el Inicio',
            error: e.errors
        });
    }
}

export async function editarInicio(req, res) {
    const {
        id,
        mision,
        vision,
        valorBasica,
        valorAvanzada,
        valorClinica,
        curacion,
        reconstruccion,
        titulo,
        subTitulo
    } = req.body;
    if (mision && vision && valorBasica && valorAvanzada && valorClinica && curacion && reconstruccion && titulo && subTitulo) {
        try {
            let inicio = await Inicio.findOne({
                where: {
                    id: id
                },
                attributes: ['id', 'mision', 'vision', 'valorBasica', 'valorAvanzada', 'valorClinica', 'curacion', 'reconstruccion', 'titulo', 'subTitulo']
            });
            if (inicio) {
                inicio.update({
                    mision,
                    vision,
                    valorBasica,
                    valorAvanzada,
                    valorClinica,
                    curacion,
                    reconstruccion,
                    titulo,
                    subTitulo
                });
                res.json({
                    code: 200,
                    message: 'Se ha editado el inicio con exito',
                    data: inicio
                });
            } else {
                res.json({
                    code: 400,
                    message: 'No existe inicio'
                });
            }
        } catch (e) {
            res.json({
                code: 401,
                message: 'ERROR'
            });
        }
    }
}