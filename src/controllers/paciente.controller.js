import Comuna from '../models/comuna';
import FichaClinica from '../models/ficha_clinica';
import Paciente from '../models/paciente';
import Usuario from '../models/usuario';

export async function crearPaciente(req, res){
    const {nombres,apellido_paterno,apellido_materno,correo,rut,telefono,comuna_id} = req.body;
    try{
        let nuevoPaciente = await Paciente.create({
            nombres,apellido_paterno,apellido_materno,correo,rut,telefono,comuna_id
        })
        if(nuevoPaciente){
            res.json({
                code:200,
                message: 'Paciente Creado Con Exito',
                data : nuevoPaciente
            });
        }
    }catch(e){
        res.json({
            code:400,
            message: 'Error al crear al Paciente',
            error: e.errors
        });
    }
}


export async function listarPaciente(req, res){
    let Pacientes = await Paciente.findAll({
        include: [{
            model: FichaClinica,
            attributes:['id']
        },{
            model: Comuna,
            attributes:['nombre']
        }],
        attributes:['id', 'nombres','apellido_paterno','apellido_materno','correo','rut','telefono'],
        order:[['id','DESC']]
    });
    res.json({
        code:200,
        message: 'Pacientes Listados Con Exito',
        Pacientes : Pacientes
    });
}

export async function buscarPaciente(req, res){
    const {id} = req.body;
    try{
        let paciente = await Paciente.findOne({
            where:{id:id},
            include: [{
                model: Comuna,
                attributes:['nombre']
            }],
            attributes:['id',  'nombres','apellido_paterno','apellido_materno','correo','rut','telefono']
        });
        if(paciente){
            res.json({
                code:200,
                message: 'El Paciente ha sido encontrado con exito',
                data : paciente
            });
        }else{
            res.json({
                code:400,
                message: 'El Paciente no existe'
            });
        }
    }catch(e){
        res.json({
            code:401,
            message: 'ERROR'
        });
    }
}

export async function buscarPacientePorNombre(req, res){
    const {nombres} = req.body;
    try{
        let paciente = await Paciente.findAll({
            
            include: [{
                where:{nombres:nombres},
                model: Comuna,
                attributes:['nombre']
            },{
                model: FichaClinica,
                attributes:['id']
            }],
            attributes:['id',  'nombres','apellido_paterno','apellido_materno','correo','rut','telefono']
        });
        if(paciente){
            res.json({
                code:200,
                message: 'El Paciente ha sido encontrado con exito',
                data : paciente
            });
        }else{
            res.json({
                code:400,
                message: 'El Paciente no existe'
            });
        }
    }catch(e){
        res.json({
            code:401,
            message: 'ERROR'
        });
    }
}

export async function buscarPacientePorId(req, res){
    const {id} = req.body;
    try{
        let paciente = await Paciente.findAll({
            
            include: [{
                where:{nombres:nombres},
                model: Comuna,
                attributes:['nombre']
            },{
                model: FichaClinica,
                attributes:['id']
            }],
            attributes:['id',  'nombres','apellido_paterno','apellido_materno','correo','rut','telefono']
        });
        if(paciente){
            res.json({
                code:200,
                message: 'El Paciente ha sido encontrado con exito',
                data : paciente
            });
        }else{
            res.json({
                code:400,
                message: 'El Paciente no existe'
            });
        }
    }catch(e){
        res.json({
            code:401,
            message: 'ERROR'
        });
    }
}


export async function editarPaciente(req, res){
    const {id, nombres,apellido_paterno,apellido_materno,correo,rut,telefono} = req.body;
    if(nombres,apellido_paterno,apellido_materno,correo,rut,telefono){
        try{
            let paciente = await Paciente.findOne({
                where:{id:id},
                include: [{
                    model:Comuna ,
                    attributes:['id']
                }],
                attributes:['id','nombres','apellido_paterno','apellido_materno','correo','rut','telefono' ]
            });
            if(paciente){
                paciente.update({nombres,apellido_paterno,apellido_materno,correo,rut,telefono});
                res.json({
                    code:200,
                    message: 'El Paciente ha sido editado con exito',
                    data : paciente
                });
            }else{
                res.json({
                    code:400,
                    message: 'El Paciente no existe'
                });
            }
        }catch(e){
            res.json({
                code:401,
                message: 'ERROR'
            });
        }
    }else{
        res.json({
            code:203,
            message: 'NO HA INGRESADO CAMPOS PARA EDITAR'
        });
    }
}


export async function buscarPacientePorRut(req, res){
    const {rut} = req.body;
    try{
        let paciente = await Paciente.findOne({
            where:{rut:rut},
            include: [{
                model: Comuna,
                attributes:['nombre']
            },{
                model: FichaClinica,
                attributes:['id']
            }],
            attributes:['id','nombres','apellido_paterno','apellido_materno','correo','rut','telefono']
        });
        if(paciente){
            res.json({
                code:200,
                message: 'El Paciente ha sido encontrado con exito',
                data : paciente
            });
        }else{
            res.json({
                code:400,
                message: 'El Paciente no existe'
            });
        }

    }catch(e){
        res.json({
            code:401,
            message: 'ERROR'
            
        });
    }
}

