import Diagnostico from "../models/diagnostico";

export async function crearDiagnostico(req, res) {
    const { motivo_consulta,diagnostico,fecha,ficha_clinica_id } = req.body;
    try {
        let nuevoDiagnostico = await Diagnostico.create({
            motivo_consulta,
            diagnostico,
            fecha, 
            ficha_clinica_id
        });
        if (nuevoDiagnostico) {
            res.json({
                code: 200,
                message: 'Diagnostico creado correctamente',
                data: nuevoDiagnostico
            });
        }

    } catch (e) {
        res.json({
            code: 400,
            message: 'Error al crear el diagnostico',
            error: e.error
        });
    }
}

export async function listarDiagnostico(req, res){
    let diagnostico= await Diagnostico.findAll({
        attributes:['id','fecha','diagnostico','motivo_consulta'],
        order:[['id','DESC']]
    });
    res.json({
        code:200,
        message: 'Listados Con Exito',
        Diagnostico : diagnostico
    });
}


export async function editarDiagnostico(req, res) {
    const {  motivo_consulta,diagnostico } = req.body;
    if ( motivo_consulta&&diagnostico&&fecha ) {
        try {
            let diagnostico = await Diagnostico.findOne({
                where: { id: id },
                attributes: ['id','fecha','diagnostico','motivo_consulta']
            });
            if (diagnostico) {
                diagnostico.update({ motivo_consulta,diagnostico});
                res.json({
                    code: 200,
                    message: 'Se ha editado el diagnostico con exito',
                    data: diagnostico
                });
            } else {
                res.json({
                    code: 400,
                    message: 'No existe diagnostico'
                });
            }
        } catch (e) {
            res.json({
                code: 401,
                message: 'ERROR'
            });
        }
    }
}