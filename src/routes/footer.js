import {Router} from 'express';
import {listarFooter,editarFooter,crearFooter} from  '../controllers/footer.controller';

const router = Router();

router.get('/listar',listarFooter );
router.post('/editar',editarFooter );
router.post('/crear',crearFooter );

export default router;