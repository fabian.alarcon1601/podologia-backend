import { Router } from "express";
import { crearDiagnostico,listarDiagnostico,editarDiagnostico} from "../controllers/diagnostico.controller";

const router = Router();

router.post('/crear', crearDiagnostico);
router.get('/listar',listarDiagnostico);
router.post('/editar', editarDiagnostico);


export default router;