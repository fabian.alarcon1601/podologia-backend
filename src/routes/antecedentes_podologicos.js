import { Router } from "express";
import { crearAntecedentesPod,listarAntecedentesPod,editarAntecedentesPod} from "../controllers/antecedentes_podologicos.controller";

const router = Router();

router.post('/crear', crearAntecedentesPod);
router.get('/listar',listarAntecedentesPod);
router.post('/editar', editarAntecedentesPod);


export default router;