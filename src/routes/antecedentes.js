import { Router } from "express";
import { crearAntecedentes,listarAntecedentes,editarAntecedentes} from "../controllers/antecedentes.controller";

const router = Router();

router.post('/crear', crearAntecedentes);
router.get('/listar',listarAntecedentes);
router.post('/editar', editarAntecedentes);


export default router;