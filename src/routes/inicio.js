import {Router} from 'express';
import {listarInicio,editarInicio,crearInicio} from  '../controllers/inicio.controller';

const router = Router();

router.get('/listar',listarInicio );
router.post('/editar',editarInicio );
router.post('/crear',crearInicio );

export default router;