import {Router} from 'express';
import { crearComuna, listarComuna } from '../controllers/comuna.controller';

const router = Router();

router.get('/listar', listarComuna);
router.post('/crear', crearComuna);

export default router;