import {Router} from 'express';
import {buscarPaciente, buscarPacientePorNombre, crearPaciente, editarPaciente, listarPaciente,buscarPacientePorRut,buscarPacientePorId} from '../controllers/paciente.controller';

const router = Router();

router.post('/crear', crearPaciente);
router.get('/listar', listarPaciente);
router.post('/buscar', buscarPaciente);
router.post('/buscar/rut', buscarPacientePorRut);
router.put('/editar', editarPaciente);
router.post('/buscar/nombre', buscarPacientePorNombre);
router.post('/paciente/buscar/usuario/id', buscarPacientePorId);



export default router;